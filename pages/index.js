import React from 'react'
import { Link } from 'react-router'
import { prefixLink } from 'gatsby-helpers'
import Helmet from "react-helmet"
import { config } from 'config'
import logo from "./Sergio-Leal.png";

export default class Index extends React.Component {
  render () {
    return (
      <div style={{ margin: '3rem auto', maxWidth: 600 }}>
        <Helmet
          title={config.siteTitle}
          meta={[
            {"name": "description", "content": "Sample"},
            {"name": "keywords", "content": "sample, something"},
          ]}
        />
        <h1>
          Hello World!
        </h1>
        <p>Welcome to your NOT SO clean Gatsby site</p>
        <img src={logo} alt="Logo" />
        <img src="https://source.unsplash.com/random/400x200" alt="" />
        <div>
	      <Link to="/page-2/">Page 2</Link>
    	</div>
        <div>
	      <Link to="/counter/">Counter</Link>
	      <Link to="/post/">Post</Link>
    	</div>
      </div>
    )
  }
}
